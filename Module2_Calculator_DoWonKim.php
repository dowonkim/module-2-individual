<!DOCTYPE html>
<html>
<head>
<TITLE>Calculator by Do Won Kim</TITLE>
</head>
<body>
Calculator by Do Won Kim<br><br>
<form name="calculator" action="?page=calculator" method="POST"> 
Number 1: <input type=text name=one><br> 
Number 2: <input type=text name=two><br> 
Operation:
<input type=radio name=sign value="add">Addition
<input type=radio name=sign value="subtract">Subtraction
<input type=radio name=sign value="divide">Division
<input type=radio name=sign value="multiply">Multiplication<br>
<input type=submit value="Calculate"> 
</form>
</body>
</html>

<?php
     class calculator {    
          var $num1; 
          var $num2; 
               function add($num1,$num2) { 
                   $answer=$num1+$num2; 
		    echo("$num1 + $num2 = $answer"); 
                    exit; 
                } 
               function subtract($num1,$num2) { 
                   $answer=$num1-$num2; 
                    echo("$num1 - $num2 = $answer"); 
                    exit; 
                } 
               function divide($num1,$num2) {
	           $answer=$num1/$num2; 
		    echo("$num1 / $num2 = $answer"); 
		    exit; 
                } 
               function multiply($num1,$num2) { 
                   $answer=$num1*$num2; 
                    echo("$num1 * $num2 = $answer"); 
                    exit; 
                } 
     }
     $calculator = new calculator(); 

     if (isset($_POST['one']) && isset($_POST['two']) && isset($_POST['sign'])){
	  $num1 = $_POST['one']; 
	  $num2 = $_POST['two']; 
	  $sign = $_POST['sign'];

	  if(!(is_numeric ($num1))) {
	       echo("Number 1 has to be a NUMBER.");
	       exit;
	  }
	  if(!(is_numeric ($num2))) {
	       echo("Number 2 has to be a NUMBER.");
	       exit;
	  }
	  if($sign == "add"){ 
	       $calculator->add($num1,$num2); 
	  }
	  if($sign == "subtract"){ 
	       $calculator->subtract($num1,$num2); 
	  }
	  if($sign == "divide"){
	       if($num2 == 0){
		    echo "Cannot divide by 0.";
		    exit;
	       } 
	       else{
		    $calculator->divide($num1,$num2); 
               }
	  }
	  if($sign == "multiply"){ 
	       $calculator->multiply($num1,$num2); 
	  }
     }
     else if(isset($_POST['one']) && isset($_POST['two']) && !(isset($_POST['sign']))){
	  echo("Select an Operation Sign.");
	  exit;
     }	 
?>